<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class PostsController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){

        $posts = Post::orderBy('id', 'desc')->paginate('10');


        return view('Posts.index')->with('posts',$posts);
    }

    public function create(){

    	return view('Posts.create');
    }

    public function save(Request $request, $post_id=null){

    	$request->validate([
                
            'title' => 'required|min:10|max:50',

            'body' => 'required',          

            'picture' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $post = Post::find($post_id);

        if($post_id==null)
        {
            $post = new post;

        } 

        $user = auth()->user();

        //dd($user->id);

        $post->user_id = $user->id;

        $post->title = $request->title;

        $post->body = $request->body;

        $imageName=$post->picture;

        if($request->hasFile('picture')){

            
            $image = $request->file('picture');

            $image_name = basename($imageName);

    		$image_path = public_path('/uploads/posts'.'/'.$image_name);
   
		    if(File::exists($image_path)){
		            
		        File::delete($image_path);
		    }

		    $extension = $image->getClientOriginalExtension();

		    $filename = rand().".".$extension;

		    $image->move(public_path().'/uploads/posts', $filename);

		    $url = url('/uploads/posts',$filename); 

            $post->picture = $url;

        }
        
        $post->save();

        return redirect(route('Posts.index'))->with('success','Post saved');


    }

    public function view($post_id) 
    {

        $post = Post::find($post_id);

        return view('Posts.view')->with('post',$post);
    }

    public function edit($post_id) 
    {
        
        $post = post::find($post_id);

        return view('Posts.edit')->with('post',$post);

    }

    public function delete($post_id) 
    {

        $post = post::find($post_id);

        if(!$post) 
        {
            return "error";
        }

        $imageName=$post->picture;

        $image_name = basename($imageName);

	    $image_path = public_path('/uploads/posts'.'/'.$image_name);
	   
	    if(File::exists($image_path)){
	            
	        File::delete($image_path);
	    }

        $post->delete();

        return redirect(route('Posts.index'))->with('success','post Removed');
    }


}
