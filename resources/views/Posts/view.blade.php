@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"> 
                	
                	<h3><strong>{{$post->title}}</strong></h3>
                </div>

                <div class="card-header">
                	Published at : {{$post->created_at}}&emsp;&emsp;Updated at : {{$post->updated_at}}
                	<a href="{{ route('Posts.edit',$post->id)}}" style="float: right;" class="btn btn-primary">Edit</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
        			<div class="alert alert-danger">
	            		<ul>
		                	@foreach ($errors->all() as $error)
		                		<li>{{ $error }}</li>
		                	@endforeach
	            		</ul>
        			</div>
       				<br/> 
        			@endif
        			<img src="{{$post->picture}}" width="1024px" height="360px"><br>
        			<br>
                    
                    <label><h4>{{$post->body}}</h4></label><br>
                    
                    <br>
                    
                
            </div>                 
              	</div>                
            </div>
        </div>
    </div>
</div>

@endsection