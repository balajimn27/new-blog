@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
        			<div class="alert alert-danger">
	            		<ul>
		                	@foreach ($errors->all() as $error)
		                		<li>{{ $error }}</li>
		                	@endforeach
	            		</ul>
        			</div>
       				<br/> 
        			@endif

                    <form action="{{route('Posts.save',$post->id)}}" method="post" enctype="multipart/form-data">
                    @method('PUT') 
                    @csrf
                    <div class="form-group">

                        <label for="title">Title:</label>
                        <input type="text" class="form-control" name="title" value="{{ $post->title }}" />
                    </div>
                   
                    <div class="form-group">
                        <label for="body">Body:</label>
                        <textarea class="form-control" name="body" >{{$post->body}}</textarea>   
                    </div>

                    <div class="form-group">
                        <label for="picture">Picture:</label>
                        <input type="file" class="form-control" name="picture" value="{{$post->picture }}"/>
                    </div>

                        <input type="submit" name="Submit" class="btn btn-primary">

                    </form>
                    </div>                


                </div>
            </div>
        </div>
    </div>
</div>

@endsection