@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{Auth::user()->name}}'s Posts </div>


                    <div class="card"><a href="{{ route('Posts.create')}}" class="btn btn-primary">Create Post</a></div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
        			<div class="alert alert-danger">
	            		<ul>
		                	@foreach ($errors->all() as $error)
		                		<li>{{ $error }}</li>
		                	@endforeach
	            		</ul>
        			</div>
       				<br/> 
        			@endif


        			<table class="table table-striped">
			    		<thead>
			        		<tr>
					          <td>Post Id</td>
					          <td>Title</td>
					          <td colspan="8">Actions</td>
        					</tr>
    					</thead>
    					<tbody>
        				@foreach(Auth::user()->posts as $post)

        					<tr>
					            <td>{{$post->id}}</td>
					            <td>{{$post->title}}</td>
           
            					<td>
                                    <a href="{{ route('Posts.view',$post->id)}}" class="btn btn-primary">View</a>
                                </td>
                                <td>
                                    <a href="{{ route('Posts.edit',$post->id)}}" class="btn btn-primary">Edit</a>
                                </td>
                                <td>
                                    <form action="{{ route('Posts.delete', $post->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" onclick="return confirm('Are you sure to delete the post ?')" type="submit">Delete</button>
                                    </form>
                                </td>
       						</tr>
                            
        				@endforeach
    					</tbody>
  					</table>                  
              	</div>                
            </div>
        </div>
    </div>
</div>



@endsection