@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create Post</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
        			<div class="alert alert-danger">
	            		<ul>
		                	@foreach ($errors->all() as $error)
		                		<li>{{ $error }}</li>
		                	@endforeach
	            		</ul>
        			</div>
       				<br/> 
        			@endif

                    <form action="{{route('Posts.save')}}" method="post" enctype="multipart/form-data">

                        {{ csrf_field() }}
                        
                        <div class="form-group">
                            
                            <label class="title">Title</label>

                            <input type="text" name="title" class="form-control" placeholder="Title" value="{{old('title')}}">

                        </div>
                        
                        <div class="form-group">
                            
                            <label class="body">Body</label>

                            <textarea name="body" class="form-control" placeholder="body"></textarea>

                        </div>
 
                        <div class="form-group">
                            
                            <label class="picture">Picture</label>

                            <input type="file" name="picture" class="form-control" value="{{old('picture')}}" id="picture" placeholder="Picture">

                        </div>

                        <input type="submit" name="Submit" class="btn btn-primary">

                    </form>
              </div>                


                </div>
            </div>
        </div>
    </div>
</div>

@endsection