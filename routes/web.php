<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/create','PostsController@create')->name('Posts.create');

Route::get('edit/{id}', 'PostsController@edit')->name('Posts.edit');

Route::get('view/{id}', 'PostsController@view')->name('Posts.view');

Route::match(array('PUT','POST'),'save/{id?}', 'PostsController@save')->name('Posts.save');

Route::get('/index', 'PostsController@index')->name('Posts.index');

Route::delete('delete/{id}', 'PostsController@delete')->name('Posts.delete');

Route::get('/home', 'HomeController@index')->name('home');

